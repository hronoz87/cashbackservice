package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CashbackServiceTest {

    @Test
    void shouldCashback() {
        int amount = 2000;
        int percent = 1;

        int expected = 20;
        CashbackService cashbackService = new CashbackService();

        int actual = cashbackService.getCashback(amount, percent);

        assertEquals(expected, actual);
    }

    @Test
    void shouldCashbackMoreMaxCashback() {
        int amount = 320000;
        int percent = 1;

        int expected = 3000;
        CashbackService cashbackService = new CashbackService();

        int actual = cashbackService.getCashback(amount, percent);

        assertEquals(expected, actual);
    }
}